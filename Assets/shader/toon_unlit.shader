// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hidden/Templates/Unlit"
{
	Properties
	{
		[NoScaleOffset]_HighlightTexture("Highlight Texture", 2D) = "white" {}
		_HighlightTiling("Highlight Tiling", Float) = 20
		_ColorHighlight("Color Highlight", Color) = (0.3705682,0.3773585,0.1940192,0)
		_Shadowspread("Shadow spread", Range( -1 , 1)) = 0
		[NoScaleOffset]_ShadowTexture("Shadow Texture", 2D) = "white" {}
		_ShadowTiling("Shadow Tiling", Float) = 6
		_ColorShadow("Color Shadow", Color) = (0.3194197,0.4911933,0.6981132,0)
		_RimLightSize("Rim Light Size", Range( 0 , 100)) = 5.072138
		_RimHighlightColour("Rim Highlight Colour", Color) = (1,0.9989318,0.2216981,0)
		_RimShadowColor("Rim Shadow Color", Color) = (0.9433962,0.08454964,0.08454964,0)
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend Off
		Cull Back
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		

		Pass
		{
			Name "Unlit"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float3 ase_normal : NORMAL;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_OUTPUT_STEREO
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			uniform float4 _RimShadowColor;
			uniform sampler2D _ShadowTexture;
			uniform sampler2D sampler02;
			uniform float4 _ShadowTexture_TexelSize;
			uniform float _ShadowTiling;
			uniform float4 _ColorShadow;
			uniform float _RimLightSize;
			uniform float4 _RimHighlightColour;
			uniform sampler2D _HighlightTexture;
			uniform sampler2D sampler06;
			uniform float _HighlightTiling;
			uniform float4 _ColorHighlight;
			uniform float _Shadowspread;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float4 ase_clipPos = UnityObjectToClipPos(v.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord = screenPos;
				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.ase_texcoord1.xyz = ase_worldPos;
				float3 ase_worldNormal = UnityObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord2.xyz = ase_worldNormal;
				
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.w = 0;
				o.ase_texcoord2.w = 0;
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed4 finalColor;
				float4 screenPos = i.ase_texcoord;
				float4 ase_screenPosNorm = screenPos/screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 blendOpSrc15 = tex2D( _ShadowTexture, ( ase_screenPosNorm * _ScreenParams * _ShadowTexture_TexelSize * _ShadowTiling ).xy );
				float4 blendOpDest15 = _ColorShadow;
				float4 temp_output_15_0 = (( blendOpDest15 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpDest15 - 0.5 ) ) * ( 1.0 - blendOpSrc15 ) ) : ( 2.0 * blendOpDest15 * blendOpSrc15 ) );
				float4 blendOpSrc37 = _RimShadowColor;
				float4 blendOpDest37 = temp_output_15_0;
				float4 _Color0 = float4(1,1,1,0);
				float3 ase_worldPos = i.ase_texcoord1.xyz;
				float3 ase_worldViewDir = UnityWorldSpaceViewDir(ase_worldPos);
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 ase_worldNormal = i.ase_texcoord2.xyz;
				float fresnelNdotV28 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode28 = ( 0.0 + _RimLightSize * pow( 1.0 - fresnelNdotV28, 2.775 ) );
				float4 temp_cast_1 = (fresnelNode28).xxxx;
				float4 lerpResult30 = lerp( _Color0 , float4( 0,0,0,0 ) , step( _Color0 , temp_cast_1 ));
				float4 lerpResult38 = lerp( ( saturate( (( blendOpDest37 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpDest37 - 0.5 ) ) * ( 1.0 - blendOpSrc37 ) ) : ( 2.0 * blendOpDest37 * blendOpSrc37 ) ) )) , temp_output_15_0 , lerpResult30);
				float4 blendOpSrc16 = tex2D( _HighlightTexture, ( ase_screenPosNorm * _ScreenParams * _ShadowTexture_TexelSize * _HighlightTiling ).xy );
				float4 blendOpDest16 = _ColorHighlight;
				float4 temp_output_16_0 = ( saturate( 2.0f*blendOpDest16*blendOpSrc16 + blendOpDest16*blendOpDest16*(1.0f - 2.0f*blendOpSrc16) ));
				float4 blendOpSrc34 = _RimHighlightColour;
				float4 blendOpDest34 = temp_output_16_0;
				float4 lerpResult32 = lerp( ( saturate( (( blendOpDest34 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpDest34 - 0.5 ) ) * ( 1.0 - blendOpSrc34 ) ) : ( 2.0 * blendOpDest34 * blendOpSrc34 ) ) )) , temp_output_16_0 , lerpResult30);
				float3 worldSpaceLightDir = UnityWorldSpaceLightDir(ase_worldPos);
				float dotResult21 = dot( worldSpaceLightDir , ase_worldNormal );
				float4 lerpResult17 = lerp( lerpResult38 , lerpResult32 , step( _Shadowspread , dotResult21 ));
				
				
				finalColor = lerpResult17;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=15800
334;92;793;541;5834.495;4070.16;10.84271;True;False
Node;AmplifyShaderEditor.ScreenParams;8;-2322.742,-1143.999;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;6;-2394.662,-981.5246;Float;False;12;1;0;SAMPLER2D;sampler06;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;3;-2340.261,-818.3538;Float;False;Property;_HighlightTiling;Highlight Tiling;1;0;Create;True;0;0;False;0;20;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;5;-2362.855,-1310.564;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;2;-2316.838,352.0214;Float;False;12;1;0;SAMPLER2D;sampler02;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;1;-2253.848,515.3962;Float;False;Property;_ShadowTiling;Shadow Tiling;5;0;Create;True;0;0;False;0;6;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;4;-2284.713,22.44074;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenParams;7;-2243.545,192.7924;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;25;-2439.517,-439.8326;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1990.503,-1111.874;Float;False;4;4;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-1918.367,91.7961;Float;False;4;4;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-2507.934,-272.5621;Float;False;Property;_RimLightSize;Rim Light Size;7;0;Create;True;0;0;False;0;5.072138;5.072138;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;-1511.149,-944.1387;Float;False;Property;_ColorHighlight;Color Highlight;2;0;Create;True;0;0;False;0;0.3705682,0.3773585,0.1940192,0;1,0.9174504,0.5896226,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;27;-2136.034,-640.5644;Float;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;28;-2213.183,-456.5455;Float;True;Standard;WorldNormal;LightDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;10;False;3;FLOAT;2.775;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;13;-1560.963,-1137.074;Float;True;Property;_HighlightTexture;Highlight Texture;0;1;[NoScaleOffset];Create;True;0;0;False;0;55cb20726a0259b46b10690562d50885;b8dc31bcdf4941942aad93a4dce19dbb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;12;-1487.653,63.61654;Float;True;Property;_ShadowTexture;Shadow Texture;4;1;[NoScaleOffset];Create;True;0;0;False;0;d731b8e490fa7324fab8af68571ca1b3;d731b8e490fa7324fab8af68571ca1b3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;14;-1431.949,258.806;Float;False;Property;_ColorShadow;Color Shadow;6;0;Create;True;0;0;False;0;0.3194197,0.4911933,0.6981132,0;0.3686275,0.3533235,0.0745098,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;22;-1011.665,1049.838;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;23;-963.8914,1206.213;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;36;-1210.33,436.7695;Float;False;Property;_RimShadowColor;Rim Shadow Color;9;0;Create;True;0;0;False;0;0.9433962,0.08454964,0.08454964,0;0.9433962,0.08454964,0.08454964,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;35;-1104.841,-1258.783;Float;False;Property;_RimHighlightColour;Rim Highlight Colour;8;0;Create;True;0;0;False;0;1,0.9989318,0.2216981,0;1,0.9989318,0.2216981,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;15;-1165.443,63.45076;Float;False;Overlay;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;16;-1234.429,-965.209;Float;False;SoftLight;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;29;-1856.41,-456.145;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;34;-766.4531,-1181.058;Float;True;Overlay;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;37;-947.356,178.9957;Float;True;Overlay;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;21;-673.7654,1093.295;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;30;-1527.259,-508.7938;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-822.1313,968.5335;Float;False;Property;_Shadowspread;Shadow spread;3;0;Create;True;0;0;False;0;0;-0.144;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;32;-362.4179,-787.1357;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;24;-434.9631,972.5294;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;38;-634.7668,48.31266;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;17;-346.6459,-34.36554;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;261.2322,-27.5087;Float;False;True;2;Float;ASEMaterialInspector;0;1;Hidden/Templates/Unlit;0770190933193b94aaa3065e307002fa;0;0;Unlit;2;True;0;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;0;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;RenderType=Opaque=RenderType;True;2;0;False;False;False;False;False;False;False;False;False;False;0;;0;0;Standard;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;10;0;5;0
WireConnection;10;1;8;0
WireConnection;10;2;6;0
WireConnection;10;3;3;0
WireConnection;9;0;4;0
WireConnection;9;1;7;0
WireConnection;9;2;2;0
WireConnection;9;3;1;0
WireConnection;28;4;25;0
WireConnection;28;2;26;0
WireConnection;13;1;10;0
WireConnection;12;1;9;0
WireConnection;15;0;12;0
WireConnection;15;1;14;0
WireConnection;16;0;13;0
WireConnection;16;1;11;0
WireConnection;29;0;27;0
WireConnection;29;1;28;0
WireConnection;34;0;35;0
WireConnection;34;1;16;0
WireConnection;37;0;36;0
WireConnection;37;1;15;0
WireConnection;21;0;22;0
WireConnection;21;1;23;0
WireConnection;30;0;27;0
WireConnection;30;2;29;0
WireConnection;32;0;34;0
WireConnection;32;1;16;0
WireConnection;32;2;30;0
WireConnection;24;0;18;0
WireConnection;24;1;21;0
WireConnection;38;0;37;0
WireConnection;38;1;15;0
WireConnection;38;2;30;0
WireConnection;17;0;38;0
WireConnection;17;1;32;0
WireConnection;17;2;24;0
WireConnection;0;0;17;0
ASEEND*/
//CHKSM=7A2D28D5DAB9DD140CFDF36F21217C85ED3F2075