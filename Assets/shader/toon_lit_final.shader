// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "New Amplify Shader"
{
	Properties
	{
		[NoScaleOffset]_HighlightTexture("Highlight Texture", 2D) = "white" {}
		_HighlightTiling("Highlight Tiling", Float) = 20
		_HighlightTextureContrast("Highlight Texture Contrast", Range( -2 , 2)) = 1
		_ColorHighlight("Color Highlight", Color) = (0.5,0.5,0.5,0)
		_Shadowspread("Shadow spread", Range( -1 , 1)) = 0
		[NoScaleOffset]_ShadowTexture("Shadow Texture", 2D) = "white" {}
		_ShadowTiling("Shadow Tiling", Float) = 6
		_ShadowTextureContrast("Shadow Texture Contrast", Range( -2 , 2)) = 1
		_ColorShadow("Color Shadow", Color) = (0.5,0.5,0.5,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 4.6
		#pragma surface surf StandardCustomLighting keepalpha noshadow exclude_path:deferred 
		struct Input
		{
			float4 screenPos;
			float3 worldPos;
			float3 worldNormal;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _ShadowTextureContrast;
		uniform sampler2D _ShadowTexture;
		uniform sampler2D sampler05;
		uniform float4 _ShadowTexture_TexelSize;
		uniform float _ShadowTiling;
		uniform float4 _ColorShadow;
		uniform float _HighlightTextureContrast;
		uniform sampler2D _HighlightTexture;
		uniform sampler2D sampler04;
		uniform float4 _HighlightTexture_TexelSize;
		uniform float _HighlightTiling;
		uniform float4 _ColorHighlight;
		uniform float _Shadowspread;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 blendOpSrc23 = CalculateContrast(_ShadowTextureContrast,tex2D( _ShadowTexture, ( ase_screenPosNorm * _ScreenParams * _ShadowTexture_TexelSize * _ShadowTiling ).xy ));
			float4 blendOpDest23 = _ColorShadow;
			float4 blendOpSrc22 = CalculateContrast(_HighlightTextureContrast,tex2D( _HighlightTexture, ( ase_screenPosNorm * _ScreenParams * _HighlightTexture_TexelSize * _HighlightTiling ).xy ));
			float4 blendOpDest22 = _ColorHighlight;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = i.worldNormal;
			float dotResult30 = dot( ase_worldlightDir , ase_worldNormal );
			float4 lerpResult34 = lerp( (( blendOpDest23 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpDest23 - 0.5 ) ) * ( 1.0 - blendOpSrc23 ) ) : ( 2.0 * blendOpDest23 * blendOpSrc23 ) ) , ( saturate( 2.0f*blendOpDest22*blendOpSrc22 + blendOpDest22*blendOpDest22*(1.0f - 2.0f*blendOpSrc22) )) , step( _Shadowspread , dotResult30 ));
			c.rgb = lerpResult34.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 blendOpSrc23 = CalculateContrast(_ShadowTextureContrast,tex2D( _ShadowTexture, ( ase_screenPosNorm * _ScreenParams * _ShadowTexture_TexelSize * _ShadowTiling ).xy ));
			float4 blendOpDest23 = _ColorShadow;
			float4 blendOpSrc22 = CalculateContrast(_HighlightTextureContrast,tex2D( _HighlightTexture, ( ase_screenPosNorm * _ScreenParams * _HighlightTexture_TexelSize * _HighlightTiling ).xy ));
			float4 blendOpDest22 = _ColorHighlight;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = i.worldNormal;
			float dotResult30 = dot( ase_worldlightDir , ase_worldNormal );
			float4 lerpResult34 = lerp( (( blendOpDest23 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpDest23 - 0.5 ) ) * ( 1.0 - blendOpSrc23 ) ) : ( 2.0 * blendOpDest23 * blendOpSrc23 ) ) , ( saturate( 2.0f*blendOpDest22*blendOpSrc22 + blendOpDest22*blendOpDest22*(1.0f - 2.0f*blendOpSrc22) )) , step( _Shadowspread , dotResult30 ));
			o.Emission = lerpResult34.rgb;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15800
83;30;1830;1003;4451.907;2713.98;4.961337;False;False
Node;AmplifyShaderEditor.ScreenParams;8;-1228.56,243.1194;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;5;-1301.853,402.3483;Float;False;17;1;0;SAMPLER2D;sampler05;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;7;-1269.728,72.76754;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;6;-1238.863,565.7228;Float;False;Property;_ShadowTiling;Shadow Tiling;6;0;Create;True;0;0;False;0;6;6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenParams;1;-1207.501,-546.8246;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;4;-1279.421,-384.3509;Float;False;16;1;0;SAMPLER2D;sampler04;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;3;-1225.02,-221.1803;Float;False;Property;_HighlightTiling;Highlight Tiling;1;0;Create;True;0;0;False;0;20;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;2;-1247.614,-713.3906;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-903.3815,142.123;Float;False;4;4;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-875.2624,-514.7;Float;False;4;4;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-605.5295,-354.5822;Float;False;Property;_HighlightTextureContrast;Highlight Texture Contrast;2;0;Create;True;0;0;False;0;1;1;-2;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;19;-600.4229,718.4887;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;20;-552.6489,874.8638;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;17;-472.6682,113.9434;Float;True;Property;_ShadowTexture;Shadow Texture;5;1;[NoScaleOffset];Create;True;0;0;False;0;None;55cb20726a0259b46b10690562d50885;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;16;-618.056,-549.2995;Float;True;Property;_HighlightTexture;Highlight Texture;0;1;[NoScaleOffset];Create;True;0;0;False;0;None;d9e9b971dff25554ba1e697402d3b0ef;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;55;-467.7757,322.2966;Float;False;Property;_ShadowTextureContrast;Shadow Texture Contrast;7;0;Create;True;0;0;False;0;1;0.07;-2;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;56;-159.2402,125.3408;Float;True;2;1;COLOR;0,0,0,0;False;0;FLOAT;1.29;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;18;-148.6469,356.1843;Float;False;Property;_ColorShadow;Color Shadow;8;0;Create;True;0;0;False;0;0.5,0.5,0.5,0;0.2617925,0.2966981,0.3490566,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;29;-410.8892,637.1846;Float;False;Property;_Shadowspread;Shadow spread;4;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;30;-262.5233,761.9457;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;52;-309.7724,-545.1022;Float;True;2;1;COLOR;0,0,0,0;False;0;FLOAT;1.29;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;15;-308.0749,-325.7559;Float;False;Property;_ColorHighlight;Color Highlight;3;0;Create;True;0;0;False;0;0.5,0.5,0.5,0;0.2156863,0.2431373,0.2627451,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;23;117.2028,121.0117;Float;False;Overlay;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;22;97.47749,-374.5353;Float;False;SoftLight;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;33;-23.72047,641.1807;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;34;530.6271,110.9387;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;61;-697.8867,-1126.273;Float;False;Constant;_Color0;Color 0;9;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;64;144.1386,-1071.154;Float;True;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;62;-697.1573,-1292.048;Float;False;Constant;_Color1;Color 1;9;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldNormalVector;60;-717.5573,-961.7041;Float;True;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleContrastOpNode;65;-119.0073,-1066.404;Float;True;2;1;COLOR;0,0,0,0;False;0;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-445.364,-908.982;Float;False;Constant;_Float1;Float 1;9;0;Create;True;0;0;False;0;1;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;63;-408.1565,-1118.899;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;795.7989,-6.235241;Float;False;True;6;Float;ASEMaterialInspector;0;0;CustomLighting;New Amplify Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;1;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;11;0;7;0
WireConnection;11;1;8;0
WireConnection;11;2;5;0
WireConnection;11;3;6;0
WireConnection;9;0;2;0
WireConnection;9;1;1;0
WireConnection;9;2;4;0
WireConnection;9;3;3;0
WireConnection;17;1;11;0
WireConnection;16;1;9;0
WireConnection;56;1;17;0
WireConnection;56;0;55;0
WireConnection;30;0;19;0
WireConnection;30;1;20;0
WireConnection;52;1;16;0
WireConnection;52;0;54;0
WireConnection;23;0;56;0
WireConnection;23;1;18;0
WireConnection;22;0;52;0
WireConnection;22;1;15;0
WireConnection;33;0;29;0
WireConnection;33;1;30;0
WireConnection;34;0;23;0
WireConnection;34;1;22;0
WireConnection;34;2;33;0
WireConnection;64;0;65;0
WireConnection;65;1;63;0
WireConnection;65;0;67;0
WireConnection;63;0;62;0
WireConnection;63;1;61;0
WireConnection;63;2;60;2
WireConnection;0;2;34;0
WireConnection;0;13;34;0
ASEEND*/
//CHKSM=A906A7C8D71021D4CA900561B79612C57074887B