﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour {

    public float counter;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        counter++;
        transform.rotation = Quaternion.Euler(new Vector3(0, counter/20, 0));
	}
}
